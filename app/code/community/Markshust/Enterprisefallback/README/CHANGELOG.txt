Markshust_Enterprisefallback
--------------------------------------------------------------------------------
                                   CHANGELOG                                    
--------------------------------------------------------------------------------
1.0.0 (Released NOV-30-2011)
- Initial release.
1.1.0 (Released MAR-23-2012)
- Fixed bug that fell back to wrong package (enterprise/default instead of custom/default).
- Added default/default fallback so this extension can also be used on Community Edition.
